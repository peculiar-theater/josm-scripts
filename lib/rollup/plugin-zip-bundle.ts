import {bold, magenta} from "colorette";
import * as fs from "fs";
import * as path from "path";
import {Plugin} from "rollup";
import {ZipFile} from "yazl";

interface ZipOptions {
  name?: string;
  dir?: string;
  file?: string;
}

function makeOutName(): string {
  const {npm_package_name: packageName = "bundle", npm_package_version: packageVersion} = process.env;

  let outName = packageName;

  if (packageVersion !== undefined) outName += "-" + packageVersion;

  return outName;
}

export default function zip(options?: ZipOptions): Plugin {
  return {
    name: "zip-bundle",
    writeBundle({dir, file}, bundle) {
      return new Promise((resolve) => {
        const outDir = dir || path.dirname(file);
        const outZipDir = path.dirname(outDir);
        const outName = options?.name || makeOutName();
        const outZipFile = options.file || path.join(outZipDir, outName + ".zip");

        const zipFile = new ZipFile();

        for (const entry of Object.values(bundle)) {
          const fileName = path.join(outName, entry.fileName);

          process.stderr.write(magenta(`zip ${bold(outZipFile)} ← ${bold(fileName)}...\n`));

          if (entry.type === "asset") {
            zipFile.addBuffer(Buffer.from(entry.source), fileName);
          } else {
            zipFile.addFile(path.join(outDir, entry.fileName), fileName);
          }
        }

        const writeStream = fs.createWriteStream(outZipFile);
        zipFile.outputStream.pipe(writeStream);
        zipFile.end();
        writeStream.on("close", resolve);
      });
    },
  };
}
