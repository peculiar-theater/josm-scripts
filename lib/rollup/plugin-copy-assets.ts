/*
 * Somewhere there is a blend of these existing plugins:
 *  rollup-plugin-copy
 *  rollup-plugin-copy2
 *  rollup-plugin-copy-assets
 * that doesn't mangle paths in strange ways and adds files to the bundle correctly.  Having yet to find that plugin, we
 * have this:
 */

import {bold, cyan} from "colorette";
import * as fg from "fast-glob";
import * as fs from "fs";
import * as path from "path";
import {Plugin} from "rollup";

type GlobPattern = string | string[];
type SourceFileName = string;
type AssetEntry = GlobPattern | {asset: SourceFileName; fileName: string};

interface CopyOptions {
  assets: AssetEntry[];
}

type AssetInfo = {
  asset: string;
  fileName: string;
};

/*
 * Copy assets into the bundle.  Assets are placed in the bundle under their relative path from the input, or with the
 * explicitly given `fileName`.
 */
export default function copy(options: CopyOptions): Plugin {
  let assets: AssetInfo[];

  return {
    name: "copy-assets",
    buildStart({input}) {
      const inputDir = path.dirname(
        Array.isArray(input) ? input[0] : typeof input === "object" ? input[Object.keys(input)[0]] : input
      );

      assets = options.assets.flatMap((asset) => {
        if (typeof asset === "string" || Array.isArray(asset)) {
          const globbedAssets = fg.sync(asset, {onlyFiles: true, unique: true}).sort();

          if (globbedAssets.length == 0) {
            this.warn(`pattern "${asset}" did not match any files`);
          }

          return globbedAssets.map((asset) => {
            const pathUnderSrcDir = path.relative(inputDir, path.dirname(asset));

            if (pathUnderSrcDir.startsWith(".." + path.sep)) {
              this.error(
                `asset '${asset}' is not under '${inputDir}', you must specify where to put it {asset: '${asset}', fileName: ...}`
              );
            }

            const fileName = path.join(pathUnderSrcDir, path.basename(asset));
            return {asset, fileName};
          });
        } else {
          return [asset];
        }
      });

      for (const asset of assets) {
        this.addWatchFile(asset.asset);
      }
    },
    generateBundle({dir, file}) {
      const outputDir = dir || path.dirname(file);

      for (const asset of assets) {
        const dstFile = path.join(outputDir, asset.fileName);

        // Mimic how rollup prints generated files
        process.stderr.write(cyan(`asset ${bold(asset.asset)} → ${bold(dstFile)}...\n`));

        this.emitFile({
          type: "asset",
          fileName: asset.fileName,
          source: fs.readFileSync(asset.asset),
        });
      }
    },
  };
}
