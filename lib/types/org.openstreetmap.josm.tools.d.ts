declare namespace org.openstreetmap.josm.tools {
  class GBC extends java.awt.GridBagConstraints {
    constructor();
    insets(insets: number): GBC;
    insets(left: number, top: number, right: number, bottom: number): GBC;
    static eol(): GBC;
    static eop(): GBC;
    static std(): GBC;
    static std(gridx: number, gridy: number): GBC;
  } // end GBC

  class ImageProvider {
    constructor(name: string);
  } // end ImageProvider
}
