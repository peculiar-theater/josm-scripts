declare namespace org.openstreetmap.josm.spi.preferences {
  class Config {
    static getPref(): IPreferences;
  }

  interface IPreferences {
    get(key: string, def: string): string;
    put(key: string, value: string): boolean;

    getBoolean(key: string, def: boolean): boolean;
    putBoolean(key: string, value: boolean): boolean;

    getInt(key: string, def: number): number;
    putInt(key: string, value: number): boolean;

    getDouble(key: string, def: number): number;
    putDouble(key: string, value: number): boolean;
  }
}
