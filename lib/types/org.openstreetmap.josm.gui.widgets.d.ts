declare namespace org.openstreetmap.josm.gui.widgets {
  class JosmTextField extends javax.swing
    .JTextField /*implements org.openstreetmap.josm.tools.Destroyable, java.awt.event.ComponentListener, java.awt.event.FocusListener, java.beans.PropertyChangeListener*/ {
    // ...
    constructor(columns: number);
    constructor(text: string);
    constructor(text: string, columns: number);
  } // end JosmTextField
}
