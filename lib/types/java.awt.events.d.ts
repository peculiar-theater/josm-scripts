declare namespace java.awt.event {
  class ActionEvent /*extends java.awt.AWTEvent*/ {
    // ...
  } // end ActionEvent

  interface ActionListener /*extends java.util.EventListener*/ {
    (arg0: java.awt.event.ActionEvent): void;
  } // end ActionListener
}
