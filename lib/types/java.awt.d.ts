declare namespace java.awt {
  class Component /*extends java.lang.Object implements java.awt.image.ImageObserver, MenuContainer, java.io.Serializable*/ {
    // ...
  } // end Component

  class Container extends Component {
    add(comp: Component): Component;
    add(comp: Component, constraints: any): Component;
  } // end Container

  class Frame extends Window /*implements MenuContainer*/ {
    // ...
  } // end Frame

  interface LayoutManager {
    // ...
  } // end LayoutManager

  interface LayoutManager2 extends LayoutManager {
    // ...
  } // end LayoutManager2

  class GridBagConstraints /* extends java.lang.Object implements java.lang.Cloneable, java.io.Serializable*/ {
    // ...
  } // end GridBagConstraints

  class GridBagLayout /*extends java.lang.Object*/ implements LayoutManager2 /*, java.io.Serializable*/ {
    // ...
  } // end GridBagLayout
}
