declare namespace java.lang {
  class Double {
    doubleValue(): number;
  }
}

declare namespace java.util {
  class List<E> {
    listIterator(): ListIterator<E>;
    listIterator(index: number): ListIterator<E>;
  }

  class ListIterator<E> {
    add(e: E): void;
    hasNext(): boolean;
    hasPrevious(): boolean;
    next(): E;
    nextIndex(): number;
    previous(): E;
    previousIndex(): number;
    remove(): void;
    set(e: E): void;
  }
}
