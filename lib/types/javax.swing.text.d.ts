declare namespace javax.swing.text {
  class JTextComponent extends javax.swing
    .JComponent /*implements javax.swing.Scrollable, javax.accessibility.Accessible*/ {
    getText(): string;
    setText(t: string): void;
  } // end JTextComponent
}
