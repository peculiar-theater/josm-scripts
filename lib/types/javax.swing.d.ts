declare namespace javax.swing {
  class AbstractButton extends JComponent /*implements java.awt.ItemSelectable, SwingConstants*/ {
    addActionListener(l: java.awt.event.ActionListener): void;
  } // end AbstractButton

  class Icon {
    // ...
  }

  class JCheckBox extends JToggleButton /*implements javax.accessibility.Accessible*/ {
    constructor(text: string);
    isSelected(): boolean;
    setSelected(b: boolean): void;
  } // end JCheckBox

  class JComponent extends java.awt
    .Container /*implements java.io.Serializable, TransferHandler$HasGetTransferHandler*/ {
    setEnabled(enabled: boolean): void;
    setToolTipText(text: string): void;
  } // end JComponent

  class JFrame extends java.awt.Frame {
    /*implements WindowConstants, javax.accessibility.Accessible, RootPaneContainer, TransferHandler$HasGetTransferHandler*/
    // ...
  } // end JFrame

  class JLabel extends JComponent /*implements SwingConstants, javax.accessibility.Accessible*/ {
    constructor(text: string);
    getText(): string;
    setText(text: string): void;
  } // end JLabel

  class JOptionPane extends JComponent /*implements javax.accessibility.Accessible*/ {
    static showOptionDialog(
      parentComponent: java.awt.Component,
      message: any /*java.lang.Object*/,
      title: string,
      optionType: number,
      messageType: number,
      icon: javax.swing.Icon,
      options: any /*java.lang.Object*/[],
      initialValue: any /*java.lang.Object*/
    ): number;
  } // end JOptionPane

  namespace JOptionPane {
    const YES_NO_OPTION: number;
    const YES_NO_CANCEL_OPTION: number;

    const ERROR_MESSAGE: number;
    const INFORMATION_MESSAGE: number;
    const QUESTION_MESSAGE: number;
    const WARNING_MESSAGE: number;
  }

  class JPanel extends JComponent /*implements javax.accessibility.Accessible*/ {
    constructor(layout: java.awt.LayoutManager);
  } // end JPanel

  class JTextField extends javax.swing.text.JTextComponent /*implements SwingConstants*/ {
    // ...
  } // end JTextField

  class JToggleButton extends AbstractButton /*implements javax.accessibility.Accessible*/ {
    // ...
  } // end JToggleButton
}
