declare function require(module: string): any;

declare function require(module: "josm/api"): {
  Api: {
    downloadArea(
      bounds: org.openstreetmap.josm.data.IBounds | {min: {lat: number; lon: number}; max: {lat: number; lon: number}}
    ): org.openstreetmap.josm.data.osm.DataSet;
  };
};

declare namespace org.openstreetmap.josm {
  const plugins: any;
}
