declare namespace org.openstreetmap.josm.io {
  type Bounds = org.openstreetmap.josm.data.Bounds;
  type DataSet = org.openstreetmap.josm.data.osm.DataSet;
  type ProgressMonitor = org.openstreetmap.josm.gui.progress.ProgressMonitor;

  export class OsmServerReader {
    parseOsm(progressMonitor: ProgressMonitor): DataSet;
  }

  export class BoundingBoxDownloader extends OsmServerReader {
    constructor(downloadArea: Bounds);
  }

  export class GeoJSONServerReader extends OsmServerReader {
    constructor(url: string);
  }
}
