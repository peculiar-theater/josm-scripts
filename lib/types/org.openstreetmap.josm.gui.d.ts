declare namespace org.openstreetmap.josm.gui {
  class HelpAwareOptionPane /*extends java.lang.Object*/ {
    static showOptionDialog(
      parentComponent: java.awt.Component,
      msg: any /*java.lang.Object*/,
      title: string,
      messageType: number,
      icon: javax.swing.Icon,
      options: HelpAwareOptionPane.ButtonSpec[],
      defaultOption: HelpAwareOptionPane.ButtonSpec,
      helpTopic: string
    ): number;
    static showOptionDialog(
      parentComponent: java.awt.Component,
      msg: any /*java.lang.Object*/,
      title: string,
      messageType: number,
      helpTopic: string
    ): number;
  } // end HelpAwareOptionPane

  namespace HelpAwareOptionPane {
    class ButtonSpec /*extends java.lang.Object*/ {
      constructor(
        text: string,
        imageProvider: org.openstreetmap.josm.tools.ImageProvider,
        tooltipText: string,
        helpTopic: string
      );
    } // end ButtonSpec
  }

  class MainApplication /*extends java.lang.Object*/ {
    static getLayerManager(): org.openstreetmap.josm.gui.layer.MainLayerManager;
    static getMainFrame(): MainFrame;
  }

  class MainFrame extends javax.swing.JFrame {
    // ...
  }

  class Notification {
    constructor();
    constructor(msg: string);

    setContent(msg: string): Notification;
    setDuration(duration: number): Notification;
    setIcon(messageType: number): Notification;

    show(): void;
  }
}

declare namespace org.openstreetmap.josm.gui.layer {
  class Layer {
    setName(name: string): void;
    getName(): string;
    setVisible(visible: boolean): void;
    isVisible(): boolean;
    toggleVisible(): boolean;
  }

  class AbstractModifiableLayer extends Layer {
    isUploadDiscouraged(): boolean;
  }
  class AbstractOsmDataLayer extends AbstractModifiableLayer {}

  class OsmDataLayer extends AbstractOsmDataLayer {
    getDataSet(): org.openstreetmap.josm.data.osm.DataSet;
    setUploadDiscouraged(uploadDiscouraged: boolean): void;
  }

  class MainLayerManager {
    getActiveLayer(): Layer;
    getEditLayer(): OsmDataLayer;
    getActiveDataLayer(): OsmDataLayer;

    getEditDataSet(): org.openstreetmap.josm.data.osm.DataSet;
    getActiveDataSet(): org.openstreetmap.josm.data.osm.DataSet;

    getVisibleLayersInZOrder(): java.util.List<Layer>;
  }
}

declare namespace org.openstreetmap.josm.gui.progress {
  class ProgressMonitor {}

  class NullProgressMonitor extends ProgressMonitor {
    static readonly INSTANCE: ProgressMonitor;
  }
}
