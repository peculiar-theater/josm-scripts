declare namespace org.openstreetmap.josm.data {
  type LatLon = org.openstreetmap.josm.data.coor.LatLon;

  interface IBounds {
    getMin(): LatLon;
    getMinLat(): number;
    getMinLon(): number;
    getMax(): LatLon;
    getMaxLat(): number;
    getMaxLon(): number;
  }

  class Bounds implements IBounds {
    getMin(): LatLon;
    getMinLat(): number;
    getMinLon(): number;
    getMax(): LatLon;
    getMaxLat(): number;
    getMaxLon(): number;

    constructor(min: LatLon, max: LatLon);
    constructor(min: LatLon, max: LatLon, roundToOsmPrecision: boolean);
    constructor(b: LatLon);
    constructor(b: LatLon, roundToOsmPrecision: boolean);
    constructor(lat: number, lon: number, roundToOsmPrecision: boolean);
    constructor(minLat: number, minLon: number, maxLat: number, maxLon: number);
    constructor(minLat: number, minLon: number, maxLat: number, maxLon: number, roundToOsmPrecision: boolean);
    constructor(coords: number[], roundToOsmPrecision: boolean);
  }

  class Data {}
}

declare namespace org.openstreetmap.josm.data.coor {
  class LatLon /* ILatLon, Coordinates */ {
    isValid(): boolean;

    constructor(lat: number, lon: number);

    lat: number;
    lon: number;

    greatCircleDistance(other: LatLon): number;
    bearing(other: LatLon): number;

    toDisplayString(): string;

    interpolate(ll2: LatLon, proportion: number): LatLon;
    getCenter(ll2: LatLon): LatLon;

    distance(ll: LatLon): number;
    distanceSq(ll: LatLon): number;
  }
}

declare namespace org.openstreetmap.josm.data.osm {
  type LatLon = org.openstreetmap.josm.data.coor.LatLon;
  type Pair<A, B> = org.openstreetmap.josm.data.tools.Pair<A, B>;

  class AbstractPrimitive {
    getVersion(): number;
    getId(): number;
    getUniqueId(): number;
    isModified(): boolean;
  }

  class OsmPrimitive extends AbstractPrimitive {
    isSelected(): boolean;
    isHighlighted(): boolean;

    get(key: string): string;
    put(key: string, value: string): void;
  }
  class IPrimitive {}

  class BBox implements IBounds {
    constructor();
    constructor(x: number, y: number);
    constructor(a: LatLon, b: LatLon);
    constructor(copy: BBox);
    constructor(x: number, y: number, r: number);
    constructor(ax: number, ay: number, bx: number, by: number);
    // ...

    add(c: LatLon): void;
    add(a: LatLon, b: LatLon): void;
    add(other: BBox): void;

    set(xmin: number, xmax: number, ymin: number, ymax: number): void;

    addPrimitive(primitive: OsmPrimitive, extraSpace: number): void;
    addPrimitive(primitive: IPrimitive, extraSpace: number): void;

    addLatLon(latlon: LatLon, extraSpace: number): void;

    height(): number;
    width(): number;
    area(): number;

    bounds(b: BBox): boolean;
    bounds(c: LatLon): boolean;

    intersects(b: BBox): boolean;

    getTopLeft(): LatLon;
    getTopRight(): LatLon;
    getBottomRight(): LatLon;
    getBottomLeft(): LatLon;

    getMin(): LatLon;
    getMinLat(): number;
    getMinLon(): number;
    getMax(): LatLon;
    getMaxLat(): number;
    getMaxLon(): number;

    getCenter(): LatLon;
  }

  class Node extends OsmPrimitive {
    constructor();
    constructor(latlon: LatLon);

    lat: number;
    lon: number;

    getCoor(): LatLon;
  }

  class Way extends OsmPrimitive {
    addNode(offs: number, n: Node): void;
    addNode(n: Node): void;

    getNodes(): java.util.List<Node>;
    setNodes(nodes: Node[]): void;

    getNodesCount(): number;
    getNode(index: number): Node;
    getNodeId(idx: number): Node;
    getNodeIds(): Node[];

    containsNode(node: Node): boolean;

    // getNeighbours(node: Node): Set<Node>;

    isClosed(): boolean;
    isArea(): boolean;

    lastNode(): Node;
    firstNode(): Node;

    getLength(): number;
    getAngles(): java.util.List<Pair<java.lang.Double, Node>>;

    // V1 Way mixin
    nodes: Node[];
  }

  class OsmData extends Data {}

  class DataSet extends OsmData {
    addPrimitive(primitive: OsmPrimitive): void;
    addPrimitiveRecursive(primitive: OsmPrimitive): void;

    mergeFrom(from: DataSet): void;

    // V1 DataSet mixin
    query(expression: string): OsmPrimitive[];
    query(expression: string, options: object): any;
    selection: any;
  }
}

declare namespace org.openstreetmap.josm.data.tools {
  class Pair<A, B> {
    a: A;
    b: B;
  }
}
