import {downloadGeoJson} from "script-tools/osm-download";
import {layerManager} from "script-tools/layers";
import {showDownloadDialog} from "script-tools/gui-download-helpers";

/** Generate the standard name for a downloaded task grid file for the given project. */
export function taskGridFileName(project_id: string): string {
  return project_id + "-tasks.geojson";
}

/** Generate the URL to download the task grid for the given project. */
export function taskGridUrl(project_id: string): string {
  return `https://tasking-manager-tm4-production-api.hotosm.org/api/v2/projects/${project_id}/tasks/?as_file=true`;
}

const taskGridNameRe = /^(\d+)-tasks.geojson$/;

/** If the current layer looks like a task grid layer, extract the project number from it and the IDs of selected tasks. */
export function getProjectIdAndTasksFromCurrentTaskGridLayer() {
  const layer = layerManager.getEditLayer();
  if (!layer) return;

  const projectId = layer.getName().match(taskGridNameRe)?.[1];
  if (!projectId) return;

  const taskIds = layer
    .getDataSet()
    .query("selected type:way taskId:")
    .map((task) => task.get("taskId"))
    .join(" & ");
  if (taskIds.length === 0) return;

  return {projectId, taskIds};
}

const layers = /*@__PURE__*/ require("josm/layers");

/** Find an existing layer with the right name to be the Task Grid for the given project. */
export function findTaskGridLayer(project_id: string) {
  return layers.get(taskGridFileName(project_id));
}

/** Download the Task Grid GeoJSON for the given project and add it as a layer. */
export function downloadAndCreateTaskGridLayer(project_id: string) {
  return layers.addDataLayer({
    ds: downloadGeoJson(taskGridUrl(project_id)),
    name: taskGridFileName(project_id),
  });
}

/** Ask the user if they want to download the Task Grid file. */
export function guiAskDownloadTaskGrid(projectId: string, title: string) {
  const action = showDownloadDialog(
    `Expected to find a layer called "${taskGridFileName(
      projectId
    )}", but didn't.  Try to download it from the Tasking Manager?`,
    title,
    {downloadNewLayer: true}
  );

  if (action == 0) return downloadAndCreateTaskGridLayer(projectId);
  else return null;
}

/** Check the visible layers for the topmost one that looks like it came from the tasking manager, and use its project
 * ID. */
export function guessCurrentProjectIdFromLayers(): string | null {
  const layers = layerManager.getVisibleLayersInZOrder();

  const pattern = /^Boundary for tasks?: (?:\d+(?:,\d+)*) of TM Project #(\d+) |^(\d+)-tasks.geojson$/;
  let projectId: string = null;

  for (const it = layers.listIterator(); it.hasNext(); ) {
    const layer = it.next();

    const match = layer.getName().match(pattern);

    if (match) projectId = match[1] || match[2];
  }

  return projectId;
}

/** Validate and extract a project ID from a string. */
export function parseProjectId(projectId: string) {
  const match = projectId.match(/^\s*#?([0-9]+)\s*/);

  if (!match) throw new Error(`Cannot understand ${projectId} as a project ID`);

  return match[1];
}

/** Split a comma separated list of task ID sets. */
export function splitTaskIds(tasks: string): string[][] {
  function parseTaskId(str: string) {
    const task = str.match(/^#?(\d+)$/);
    if (!task) throw new Error(`expected a Task Id, not "${str}"`);
    return task[1];
  }

  tasks = tasks.replace(/\s+/g, "");

  if (tasks.length == 0) return [];

  return tasks.split(",").map((set) => set.split("&").map(parseTaskId));
}
