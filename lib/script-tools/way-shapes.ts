import {println, consoleAvailable} from "script-tools/console";

export enum Shape {
  Round,
  Square,
  Roundish,
  Squarish,
  Other,
}

export type ClassifyWayConfig = {
  square_ratio: number;
  squarish_ratio: number;
  round_ratio: number;
  roundish_ratio: number;
};

class SummaryStats {
  min = undefined as number;
  max = undefined as number;
  sum = 0;
  sum_sq = 0;
  num = 0;

  add(value: number): void {
    this.num++;
    this.sum += value;
    this.sum_sq += value * value;

    if (this.min === undefined) {
      this.min = value;
      this.max = value;
    } else {
      if (value < this.min) this.min = value;
      if (value > this.max) this.max = value;
    }
  }

  summary() {
    const avg = this.sum / this.num;

    return {
      min: this.min,
      avg: avg,
      var: (this.sum_sq - (this.sum * this.sum) / this.num) / (this.num - 1),
      max: this.max,
      sum: this.sum,
      num: this.num,
    } as const;
  }
}

type Way = org.openstreetmap.josm.data.osm.Way;

function summarize_way_angles(way: Way, trace: boolean) {
  const summarizer = new SummaryStats();
  let max_difference_from_90: number = undefined;

  const angles = way.getAngles();

  for (const it = angles.listIterator(); it.hasNext(); ) {
    const angle = it.next().a.doubleValue();

    if (trace) println(`  [${summarizer.num}] ${angle.toFixed(4)}°`);

    const angle_mod_90 = angle % 90;
    const difference_from_90 = angle_mod_90 < 45 ? angle_mod_90 : 90 - angle_mod_90;

    summarizer.add(angle);

    if (max_difference_from_90 === undefined) {
      max_difference_from_90 = difference_from_90;
    } else {
      if (difference_from_90 > max_difference_from_90) max_difference_from_90 = difference_from_90;
    }
  }

  const summary = summarizer.summary();
  (summary as any).max_difference_from_90 = max_difference_from_90;

  return summary as typeof summary & {max_difference_from_90: number};
}

function summarize_way_lengths(way: Way, trace: boolean) {
  type LatLon = org.openstreetmap.josm.data.coor.LatLon;

  const summarizer = new SummaryStats();

  const nodes = way.getNodes();

  let last_latlon: LatLon = null;

  // NB: for a closed way, the same Node is the first and last Node, so we will get all the edges without any
  // additional special handling for the first or last Node.
  for (const it = nodes.listIterator(); it.hasNext(); ) {
    const node = it.next();
    const latlon = node.getCoor();

    if (!latlon) throw "unable to get coordinates for node‽";

    if (last_latlon !== null) {
      const length = latlon.greatCircleDistance(last_latlon);

      if (trace) println(`  [${summarizer.num}] ${length.toFixed(4)}m`);

      summarizer.add(length);
    }

    last_latlon = latlon;
  }

  return summarizer.summary();
}

function min_max_exceed(summary: {min: number; avg: number; max: number}, ratio: number) {
  const threshold = summary.avg * ratio;
  return summary.max > summary.avg + threshold || summary.min < summary.avg - threshold;
}

export function classify_way(way: Way, config: ClassifyWayConfig): Shape {
  const trace = consoleAvailable && way.isSelected();

  if (trace) println(`Way ${way.getUniqueId()} v${way.getVersion()} has ${way.getNodesCount()} nodes`);

  /*
   * Reject buildings that have less than 4 corners or aren't closed.  (NB: A closed way always includes one node
   * twice, so a closed way with 4 corners has 5 nodes: A B C D A)
   */
  if (way.getNodesCount() < 5 || !way.isClosed()) {
    if (trace) println("  = Other (node count or open)");
    return Shape.Other;
  }

  /*
   * First pass: check for squareness
   *
   * Squareness requires:
   *  - all angles are either 90° (corner) or 180° (a extra node in an otherwise straight segment)
   *
   * We can also reject any building with an angle ≪90° because it can't be square or round.  (For round, the
   * angles must all be > 90 because of the minimum node requirement.)
   */

  const angles = summarize_way_angles(way, trace);

  const square_threshold = 90 * config.square_ratio;
  const squarish_threshold = 90 * config.squarish_ratio;

  if (trace) {
    const std = Math.sqrt(angles.var);
    println(
      `  angles: min: ${angles.min.toFixed(3)}° avg: ${angles.avg.toFixed(
        3
      )}° max_difference_from_90: ${angles.max_difference_from_90.toFixed(3)}°`
    );
    println(`  angles: max: ${angles.max.toFixed(3)}° std: ${std.toFixed(3)}°`);
  }

  if (angles.min < 90 - squarish_threshold) {
    if (trace) println("  = Other (angle too small)");
    return Shape.Other;
  }

  if (angles.max_difference_from_90 < square_threshold) {
    if (trace) println("  = Square");
    return Shape.Square;
  }

  if (angles.max_difference_from_90 < squarish_threshold) {
    if (trace) println("  = Squarish");
    return Shape.Squarish;
  }

  /*
   * If not squarish, second pass: check for roundness.
   *
   * Roundness requires:
   *  - all angles the same, and
   *  - all way lengths the same
   *
   * A single angle that's off is sufficient to disqualify from a class of roundness.  If such an angle exists, it
   * must be either the min angle or the max angle, and we already have those from the first pass.
   *
   * So, start by checking to see if way cannot possibly be Round or Roundish.
   */

  if (min_max_exceed(angles, config.roundish_ratio)) {
    if (trace) println("  = Other (not Roundish, angles)");
    return Shape.Other;
  }

  const lengths = summarize_way_lengths(way, trace);

  if (trace) {
    const std = Math.sqrt(lengths.var);
    println(`  lengths: min: ${lengths.min.toFixed(3)}m avg: ${lengths.avg.toFixed(3)}m`);
    println(`  lengths: max: ${lengths.max.toFixed(3)}m std: ${std.toFixed(3)}m`);
  }

  // Second, check that the Way is still at least roundish on the lengths.
  if (min_max_exceed(lengths, config.roundish_ratio)) {
    if (trace) println("  = Other (not Roundish, lengths)");
    return Shape.Other;
  }

  /*
   * It's beginning to look at least Roundish.  Use the diameter to estimate the radius and how many nodes JOSM
   * would use for the circle.  iD always uses more nodes, so we can use the JOSM number as a lower bound on how
   * many nodes a circle should have.
   *
   *  D = 2πr  => r = D / 2π
   *
   * JOSM uses 6 nodes or:
   *
   *  N = 6 * sqrt(r) = 6 * sqrt(D / 2π)
   *
   * which ever is greater.  It would also round up N to an even number, but we don't need to bother because we'll
   * be adding an allowance and it's only a lower bound.
   */
  const num_circle_nodes = Math.max(
    6,
    Math.floor(6 * Math.pow(lengths.sum / (2 * Math.PI), 0.5) * (1 - config.round_ratio))
  );

  if (angles.num < num_circle_nodes) {
    if (trace) println(`  = Roundish (not Round, wanted >= ${num_circle_nodes} nodes)`);
    return Shape.Roundish;
  }

  if (min_max_exceed(angles, config.roundish_ratio)) {
    if (trace) println("  = Roundish (not Round, angles)");
    return Shape.Roundish;
  }

  if (min_max_exceed(lengths, config.round_ratio)) {
    if (trace) println("  = Roundish (not Round, lengths)");
    return Shape.Roundish;
  }

  // No reasons left not to call it round

  if (trace) println("  = Round (not Roundish)");
  return Shape.Round;
}
