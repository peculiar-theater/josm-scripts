import * as errors from "script-tools/errors";

/** Run a script catching any errors and telling the user about them. */
export default function runScript(scriptName: string, script: () => void) {
  try {
    script();
  } catch (e) {
    if (e instanceof errors.SoftError) {
      new org.openstreetmap.josm.gui.Notification(e.message).setIcon(javax.swing.JOptionPane.ERROR_MESSAGE).show();
    } else {
      require("josm").alert(e, {title: scriptName, messageType: "error"});
    }
  }
}
