/** Is the console open?  For example, so that we can print stuff to it. */
export const consoleAvailable = (function () {
  try {
    // Throws if the console is not open.
    org.openstreetmap.josm.plugins.scripting.ui.console.ScriptingConsole.instance.scriptLog;

    return true;
  } catch (e) {
    return false;
  }
})();

/** Print a line to the scripting console if it was open, otherwise nothing.  Console printing only works if the console
 * is open, and crashes otherwise.  We don't want to crash just because the console is not open so drop in a stub print
 * function if necessary. */
export const println = consoleAvailable ? require("josm/scriptingconsole").println : function () {};
