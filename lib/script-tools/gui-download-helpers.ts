export function showDownloadDialog(
  msg: any,
  title: string,
  options?: {download?: boolean; downloadNewLayer?: boolean}
) {
  function makeButton(label: string, image: string, toolTip: string) {
    return new org.openstreetmap.josm.gui.HelpAwareOptionPane.ButtonSpec(
      label,
      new org.openstreetmap.josm.tools.ImageProvider(image),
      toolTip,
      null
    );
  }

  const buttons = [];

  if (options?.download) {
    buttons.push(makeButton("Download", "download", "Click to download"));
  }

  if (options?.downloadNewLayer) {
    buttons.push(makeButton("Download as new layer", "download_new_layer", "Click to download into a new data layer"));
  }

  buttons.push(makeButton("Cancel", "cancel", "Click to cancel"));

  const action = org.openstreetmap.josm.gui.HelpAwareOptionPane.showOptionDialog(
    org.openstreetmap.josm.gui.MainApplication.getMainFrame(),
    msg,
    title,
    javax.swing.JOptionPane.QUESTION_MESSAGE,
    null,
    buttons,
    buttons[0],
    null
  );

  return action;
}
