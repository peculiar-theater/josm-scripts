/* Alias and reexport the types for convenience. */
export type DataSet = org.openstreetmap.josm.data.osm.DataSet;
export type Bounds = org.openstreetmap.josm.data.Bounds;
export type IBounds = org.openstreetmap.josm.data.IBounds;

const Bounds = org.openstreetmap.josm.data.Bounds;

type ProgressMonitor = org.openstreetmap.josm.gui.progress.ProgressMonitor;

const nullProgressMonitor: ProgressMonitor = org.openstreetmap.josm.gui.progress.NullProgressMonitor.INSTANCE;

/** Download a GeoJSON file and return it as a DataSet. */
export function downloadGeoJson(url: string, progress = nullProgressMonitor): DataSet {
  return new org.openstreetmap.josm.io.GeoJSONServerReader(url).parseOsm(progress);
}

/** Download data for the given bounding box, returning it as a DataSet */
export function downloadArea(bounds: IBounds, progress = nullProgressMonitor): DataSet {
  if (!(bounds instanceof Bounds)) {
    bounds = new Bounds(bounds.getMinLat(), bounds.getMinLon(), bounds.getMaxLat(), bounds.getMaxLon());
  }
  return new org.openstreetmap.josm.io.BoundingBoxDownloader(bounds).parseOsm(progress);
}

/** Not a real downloader, instead creates a DataSet with a rectangular Way representing the area that would have been
 * downloaded. */
export function downloadBounds(bounds: IBounds): DataSet {
  const Node = org.openstreetmap.josm.data.osm.Node;
  const Way = org.openstreetmap.josm.data.osm.Way;
  const DataSet = org.openstreetmap.josm.data.osm.DataSet;
  const LatLon = org.openstreetmap.josm.data.coor.LatLon;

  const corners = [
    new Node(new LatLon(bounds.getMinLat(), bounds.getMinLon())),
    new Node(new LatLon(bounds.getMinLat(), bounds.getMaxLon())),
    new Node(new LatLon(bounds.getMaxLat(), bounds.getMaxLon())),
    new Node(new LatLon(bounds.getMaxLat(), bounds.getMinLon())),
  ];

  const way = new Way();
  way.setNodes(corners);
  way.addNode(corners[0]); // close the way

  const dataSet = new DataSet();

  corners.forEach((n) => dataSet.addPrimitive(n));
  dataSet.addPrimitive(way);

  return dataSet;
}
