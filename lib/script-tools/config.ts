type MappedType = string | boolean | number;

type DefaultConfig = {[key: string]: MappedType};

/** Load configuration from JOSM's preferences, using the defaults as a template. */
export function getConfig<T extends DefaultConfig>(keyNamespace: string, defaults: T): T {
  const prefs = org.openstreetmap.josm.spi.preferences.Config.getPref();

  const a = Object.keys(defaults).reduce((config, key) => {
    const def = defaults[key];

    const prefKey = `${keyNamespace}.${key}`;

    if (typeof def === "boolean") config[key] = prefs.getBoolean(prefKey, def);
    else if (typeof def === "number") config[key] = prefs.getDouble(prefKey, def);
    else config[key] = prefs.get(prefKey, def);

    return config;
  }, {}) as T;

  return a;
}

/** Write configuration into JOSM's preferences. */
export function putConfig<T extends DefaultConfig>(keyNamespace: string, config: T): void {
  const prefs = org.openstreetmap.josm.spi.preferences.Config.getPref();

  Object.keys(config).forEach((key) => {
    const value = config[key];

    const prefKey = `${keyNamespace}.${key}`;

    if (typeof value === "boolean") prefs.putBoolean(prefKey, value);
    else if (typeof value === "number") prefs.putDouble(prefKey, value);
    else prefs.put(prefKey, value);

    return config;
  });
}
