/** An error to do with something easily fixed user. */
export class SoftError extends Error {
  constructor(message: string) {
    super(message);

    // Set the prototype explicitly to ensure that `instanceof` works.  setPrototypeOf seems to be an ES6 thing that
    // doesn't downlevel to ES5 so try to do something graceful.
    (
      Object.setPrototypeOf ||
      function (o, proto) {
        o.__proto__ = proto;
      }
    )(this, SoftError.prototype);
  }
}
