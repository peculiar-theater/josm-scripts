/*
 * Helpers for working with JOSM's layers.  There's nothing particularly complex, but some things get a bit wordy and
 * it's nice to have shorter names for them.
 */

import {SoftError} from "script-tools/errors";

export const layerManager = org.openstreetmap.josm.gui.MainApplication.getLayerManager();

/** Get the current editable data set, or die trying.
 * @throws {SoftError} if there is no edit data set. */
export function getEditDataSet(): org.openstreetmap.josm.data.osm.DataSet {
  const data = layerManager.getEditDataSet();

  if (data === null)
    throw new SoftError("No editable layer active.  Try choosing an editable data layer as the active layer.");

  return data;
}

/** Get the current data set.
 * @throws {SoftError} If there is no active data set. */
export function getActiveDataSet(): org.openstreetmap.josm.data.osm.DataSet {
  const data = layerManager.getActiveDataSet();

  if (data === null) throw new SoftError("No data layer selected.  Try choosing a data layer as the active layer.");

  return data;
}
