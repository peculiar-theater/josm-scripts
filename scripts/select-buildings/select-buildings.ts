const scriptName = "Select Buildings by Shape";

import {println} from "script-tools/console";
import {SoftError} from "script-tools/errors";
import {getActiveDataSet} from "script-tools/layers";
import {Shape, ClassifyWayConfig, classify_way} from "script-tools/way-shapes";

type ScriptConfig = ClassifyWayConfig & {query: string};

const defaultConfig: ScriptConfig = {
  query: "type:way building:",
  round_ratio: 0.011111,
  square_ratio: 0.011111,
  roundish_ratio: 0.166666,
  squarish_ratio: 0.166666,
} as const;

type Way = org.openstreetmap.josm.data.osm.Way;
type Buildings = Way[];

function find_buildings({query}: {query: string}, dataset: org.openstreetmap.josm.data.osm.DataSet): Buildings {
  const buildings = dataset.query(query) as Buildings;

  if (buildings.length == 0) {
    throw new SoftError("No buildings found");
  }

  return buildings;
}

function group_buildings_by_shape(config: ClassifyWayConfig, buildings: Buildings): Buildings[] {
  println(`Found ${buildings.length} buildings`);

  return buildings.reduce((sets, building) => {
    (sets[classify_way(building, config)] ||= []).push(building);
    return sets;
  }, []);
}

/** Ask the user user which of the detected types of buildings they want to select. */
function ui_choose_buildings_by_shape(buildings_by_shape: Buildings[]) {
  const options = [];
  const option_shapes = [];
  let num_buildings = 0;

  buildings_by_shape.forEach((set, shape) => {
    const n = set.length;
    if (n > 0) {
      num_buildings += n;
      options.push(`${Shape[shape]} (${n})`);
      option_shapes.push(shape);
    }
  });

  options.push("Cancel");

  const JOptionPane = javax.swing.JOptionPane;
  const action = JOptionPane.showOptionDialog(
    null,
    `Classified ${num_buildings} buildings.  Select which ones?`,
    scriptName,
    JOptionPane.YES_NO_CANCEL_OPTION,
    JOptionPane.QUESTION_MESSAGE,
    null,
    options,
    options[options.length - 1]
  );

  return buildings_by_shape[option_shapes[action]];
}

import runScript from "script-tools/script";
import {getConfig} from "script-tools/config";

runScript(scriptName, () => {
  const config = getConfig("select_buildings", defaultConfig);
  const current_dataset = getActiveDataSet();
  const buildings = find_buildings(config, current_dataset);
  const buildings_by_shape = group_buildings_by_shape(config, buildings);
  const selection = ui_choose_buildings_by_shape(buildings_by_shape);

  if (selection && selection.length > 0) {
    current_dataset.selection.set(selection);
  }
});
