# select-buildings script

This script processes buildings on the current layer, attempts to classify them by their shape, and then allows you to
select buildings based on this classification.  The classifications are:

 - Round: the building is a perfect circle
 - Square: the building's angles are all 90° (corners) or 180° (middle nodes in line segment)
 - Roundish: the building is close to being circular, but is a bit off
 - Squarish: the building's angles are close to square, but not quite
 - Other: any building with less than four nodes, is not closed, or cannot be classified as one of the other four types

![An option dialog showing the results of classification and asking which shapes to select](image.jpg)

Generally speaking, Round and Square buildings would not be visibly modified by JOSM's "Align Nodes in Circle" (`O`) or
"Orthogonalize Shape" (`Q`) commands, but Roundish and Squarish buildings will probably benefit from the appropriate
command.

## Configuration

The script reads preferences from under `select_buildings`.

| key               | default               | description |
| :--               | :-----                | :---------- |
| `query`           | `type:way building:`  | Use this search string to find buildings.  The query must produce ways or bad things will probably happen. |
| `square_ratio`    | `0.011111`       | How close the angles of a way must be to 90° or 180° to qualify as Square. |
| `squarish_ratio`  | `0.166666`       | How close the angles of a way must be to 90° or 180° to qualify as Squarish. |
| `round_ratio`     | `0.011111`       | How close the angles and lengths of a way must each other to qualify as Round. |
| `roundish_ratio`  | `0.166666`       | How close the angles and lengths of a way must each other to qualify as Roundish. |

You can edit these preferences under JOSM's Advanced Preferences.  For example, to be more accepting of iD's round
buildings you could try setting `select_buildings.round_ratio` to `0.05`. You must run the script at least once for the
settings to appear.

## Notes

 - The script is quite picky about what it considers to be "Round".  For example, many circular buildings that iD
   creates fail the Round test and are only Roundish because they have too much variability in their angles and edge
   lengths.  JOSM will adjust these buildings if you attempt to circularize them, so I guess that's consistent?
   Buildings created in JOSM by "Create Circle" or the buildings\_tool plugin pass the Round test with no difficulty.
   If there are too many round looking buildings that are not being classified as Round for you, try relaxing the
   `round_ratio` preference to a larger number.

## Requirements

The script requires the [JOSM] [scripting plugin] and the embedded Rhino engine with the V1 interface.

[josm]: https://josm.openstreetmap.de/
[scripting plugin]: https://gubaer.github.io/josm-scripting-plugin/
