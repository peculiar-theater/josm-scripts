const scriptName = "HOT Task Downloader";

import * as hot from "script-tools/hot-tasks";
import {downloadArea, downloadBounds} from "script-tools/osm-download";
import {showDownloadDialog} from "script-tools/gui-download-helpers";

type ScriptConfig = {
  project_id: string;
  task_ids: string;
  debug?: boolean;
};

const configKey = "download_hot_tasks";
const defaultConfig: ScriptConfig = {
  project_id: "",
  task_ids: "",
  debug: false,
};

function guiGetTasks(config: ScriptConfig) {
  const GBC = org.openstreetmap.josm.tools.GBC;
  const JLabel = javax.swing.JLabel;
  const JCheckBox = javax.swing.JCheckBox;
  const JosmTextField = org.openstreetmap.josm.gui.widgets.JosmTextField;

  const panel = new javax.swing.JPanel(new java.awt.GridBagLayout());

  const projectIdField = new JosmTextField(6);
  projectIdField.setText(config.project_id);

  const tasksField = new JosmTextField(30);
  tasksField.setToolTipText("Comma separated list of tasks to download");
  tasksField.setText(config.task_ids);

  const autoProjectIdCheckBox = new JCheckBox("Guess from active layers");
  autoProjectIdCheckBox.setToolTipText("Guess the project ID from the currently active layers");

  autoProjectIdCheckBox.addActionListener((e) => {
    const getProjectIdFromLayers = autoProjectIdCheckBox.isSelected();
    if (getProjectIdFromLayers) projectIdField.setText(hot.guessCurrentProjectIdFromLayers());
    projectIdField.setEnabled(!getProjectIdFromLayers);
  });

  panel.add(new JLabel("Project ID:"), GBC.std());
  const projectIdPanel = new javax.swing.JPanel(new java.awt.GridBagLayout());
  projectIdPanel.add(projectIdField, GBC.std());
  projectIdPanel.add(autoProjectIdCheckBox, GBC.eol());
  panel.add(projectIdPanel, GBC.eol().insets(5, 5, 5, 5));

  panel.add(new JLabel("Tasks:"), GBC.std());
  panel.add(tasksField, GBC.eol().insets(5, 5, 5, 5));

  const action = showDownloadDialog(panel, scriptName, {downloadNewLayer: true});

  if (action == 0) {
    const projectId = hot.parseProjectId(projectIdField.getText());
    const taskIdSets = hot.splitTaskIds(String(tasksField.getText()));
    return {projectId, taskIdSets};
  } else {
    return null;
  }
}

/** Get BBoxes to download for the given sets of taskIds.
 *
 * We generate the areas using BBoxes, which are axis-aligned in LatLon space.  This means that the download area may
 * not exactly match the task's boundary.  For some tasks this is obviously to be expected because, for example, the
 * task touches the project's AOI and the resulting task boundary from the intersection of the two areas is clearly not
 * square.  But some tasks can look square to the eye but in reality not be perfectly square or axis-aligned.  For
 * example, task 86 from project 12345 looks like a nice box but has corners like this:
 *   corners on task box   corners of BBox
 *   -5.089819, 32.732926  -5.094049*, 32.728091*
 *   -5.094036, 32.732942  -5.094049*, 32.732942
 *   -5.094049, 32.728107  -5.089819*, 32.732942*
 *   -5.089833, 32.728091  -5.089819*, 32.728091
 * BBox coordinates marked "*" do not match the coordinate of the task box corresponding corner of the task box.
 * However, examining the coordinates of the original corners reveals the task box is not actually axis aligned and
 * therefor that the corners of the BBox will not match.  The generated BBox is correct and corresponds to the area that
 * JOSM will download so this is all consistent, but if you're wondering why the download area doesn't quite line up
 * with the task box, this might be why. */
function getTaskBBoxes(tasksGridData, taskIdSets: string[][]) {
  var bboxes = [];

  for (const taskIdSet of taskIdSets) {
    const bbox = taskIdSet
      .map((taskId) => {
        const objects = tasksGridData.query(`taskId=${taskId}`);

        if (objects.length == 0) throw `Couldn't find boundary for task ${taskId}`;
        if (objects.length != 1) throw `Found more than 1 boundary for task ${taskId}`;

        return objects[0].getBBox();
      })
      .reduce((a, b) => {
        a.add(b);
        return a;
      }, new org.openstreetmap.josm.data.osm.BBox());

    bboxes.push(bbox);
  }

  return bboxes;
}

function getConfigFromCurrentLayer(): ScriptConfig {
  const tasks = hot.getProjectIdAndTasksFromCurrentTaskGridLayer();
  if (!tasks) return undefined;
  else return {project_id: tasks.projectId, task_ids: tasks.taskIds};
}

type DataSet = org.openstreetmap.josm.data.osm.DataSet;
function mergeDataSet(a: DataSet, b: DataSet): DataSet {
  a.mergeFrom(b);
  return a;
}

import runScript from "script-tools/script";
import {getConfig, putConfig} from "script-tools/config";

runScript(scriptName, () => {
  const config = getConfig(configKey, defaultConfig);

  const download = guiGetTasks(getConfigFromCurrentLayer() ?? config);

  if (!download) return; // The user cancelled.

  putConfig<ScriptConfig>(configKey, {
    project_id: download.projectId,
    task_ids: download.taskIdSets.map((ids) => ids.join("&")).join(", "),
  });

  const {projectId, taskIdSets} = download;
  const taskGridLayer = hot.findTaskGridLayer(projectId) || hot.guiAskDownloadTaskGrid(projectId, scriptName);

  if (!taskGridLayer) return; // The user cancelled the layer download.
  if (taskIdSets.length == 0) return;

  const downloader = config.debug ? (a) => downloadBounds(a) : (a) => downloadArea(a);
  const data = getTaskBBoxes(taskGridLayer.data, taskIdSets).map(downloader).reduce(mergeDataSet);

  const newLayerName = `${projectId}: ${taskIdSets.join(",")}`;
  const layers = require("josm/layers");
  layers.addDataLayer({name: newLayerName, ds: data});
});
