# download-hot-tasks script

This script downloads data for tasks from the [HOT] [Tasking Manager] directly inside [JOSM].  Run the script and enter
the project ID and task IDs that you wish to download.  Task IDs are given as a comma separated list, for example `86,
26`.  A union of task IDs can also be given using `&`, for example `121&54`.  This downloads a single area that is large
enough to contain all the specified tasks.

![](image.jpg)

The image above shows highlighted in red the three areas that will be downloaded given the tasks `86, 26, 121&54`: task
86, task 26, and the area containing tasks 121 and 54.

Guessing the project ID from active layers looks for the topmost visible layer that appears to contain a project ID (for
example, the "Boundary for … TM Project #12345" layer) and uses that.

## Notes

 - If you have not already done so, the script will also download the Tasks Grid for the project and add it as a new
   layer.  If don't give any task IDs to download, that is all the script will do.
 - If the current layer is a previously downloaded Tasks Grid with selected tasks, the project ID and IDs of the
   selected tasks will be prefilled.

## Requirements

The script requires the [JOSM] [scripting plugin] and the embedded Rhino engine with the V1 interface.

[hot]: https://www.hotosm.org/
[tasking manager]: https://tasks.hotosm.org/
[josm]: https://josm.openstreetmap.de/
[scripting plugin]: https://gubaer.github.io/josm-scripting-plugin/
