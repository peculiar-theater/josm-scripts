import typescript from "rollup-plugin-typescript2";
import {terser} from "rollup-plugin-terser";
import copy from "./lib/rollup/plugin-copy-assets.ts";
import zip from "./lib/rollup/plugin-zip-bundle.ts";
import * as path from "path";

const production = !process.env.ROLLUP_WATCH;
const packageVersion = process.env.package_version || process.env.npm_version;

/** Generate config for a script written for the Rhino v1 API. */
function v1ScriptConfig(scriptName) {
  const tsCompilerOptions = {
    module: "esnext",
    moduleResolution: "node",
    target: "es5",
    lib: ["es5"],
  };

  function input(file) {
    return path.join("scripts", scriptName, file);
  }

  function dist(file) {
    return path.join("dist", scriptName, file);
  }

  let packageName = scriptName;
  if (packageVersion != undefined) {
    packageName += "-" + packageVersion;
  }

  return {
    input: input(`${scriptName}.ts`),
    output: {
      dir: dist(""),
      format: "iife",
      banner: `/*! ${packageName} */`,
    },
    plugins: [
      typescript({compilerOptions: tsCompilerOptions}),
      production && terser(),
      copy({
        assets: [input("README.*"), input("*.jpg"), {asset: "LICENSE", fileName: "LICENSE"}],
      }),
      production && zip({name: packageName}),
    ],
  };
}

const v1Scripts = [
  "download-hot-tasks",
  "select-buildings",
];

export default v1Scripts.map(v1ScriptConfig);
