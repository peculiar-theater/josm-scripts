# peculiar theater's collection of JOSM scripts

This is a collection of [JOSM] scripts that I've written:

 - [select-buildings](scripts/select-buildings): select buildings based on their shape: squared, round or other.
 - [download-hot-tasks](scripts/download-hot-tasks): download data for tasks from the [HOT] [Tasking Manager] directly
   inside [JOSM].

## Installation

Prebuilt copies of the scripts that are ready to run can be downloaded from the latest [pipeline artifacts].  Unpack the
archive, place the `.js` file somewhere convenient, and then run it from the Scripting ▹ Run... menu item.

## Requirements

The scripts requires the [JOSM] [scripting plugin].  See the individual scripts for any more specific requirements.

[hot]: https://www.hotosm.org/
[tasking manager]: https://tasks.hotosm.org/
[pipeline artifacts]: https://gitlab.com/peculiar-theater/josm-scripts/-/jobs/artifacts/main/browse/dist?job=build
[josm]: https://josm.openstreetmap.de/
[scripting plugin]: https://gubaer.github.io/josm-scripting-plugin/
